package main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Ejecutable {
    public static void main(String args[]){
        //Collection
        List<User> users = new ArrayList<>();
        users.add(new User("Pedro", 23));
        users.add(new User("Maria", 32));
        users.add(new User("Lisa", 39));
        users.add(new User("Lourdes", 35));
        users.add(new User("Bart", 42));
        users.add(new User("Jose", 4));
        Stream<User> stream = users.stream(); //Abstraccion
        System.out.println("Stream generado a partir de un arrayList: " + stream.count());
        //Stream
        Stream<User>  filtrado = users.stream().filter( user -> user.getAge() > 18 );
        System.out.println("Stream generado a partir de un filtro: " + filtrado.count());
        //Array
        int[] numbers = {2, 4, 6, 8};
        Stream stream2 = Stream.of(numbers);
        System.out.println("Stream generado a partir de un arreglo: " + stream2.count());
        //Secuency
        Stream stream3 = Stream.of("prueba 1", "prueba 2", "Test 3", "Test 4");
        System.out.println("Stream generado a partir de una secuencia: " + stream3.count());
    }
}
