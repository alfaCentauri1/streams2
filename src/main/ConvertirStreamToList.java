package main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConvertirStreamToList {
    public static void main(String args[]) {
        //Collection
        List<User> users = new ArrayList<>();
        users.add(new User("Pedro", 23));
        users.add(new User("Maria", 32));
        users.add(new User("Lisa", 39));
        users.add(new User("Lourdes", 35));
        users.add(new User("Bart", 42));
        users.add(new User("Jose", 4));
        users.add(new User("Maggi Simpson", 2));
        Stream<User> stream = users.stream(); //Abstraccion
        System.out.println("Stream generado a partir de un arrayList: " + stream.count());
        List<User>  listado = users.stream().filter( user -> user.getAge() > 18 ).collect(Collectors.toList());
        System.out.println("Lista generada a partir de un stream: " + listado.size());
        System.out.println(listado);
    }
}
